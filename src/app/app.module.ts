import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { TableWrapperComponent } from './components/table-wrapper/table-wrapper.component';
import { TableComponentComponent } from './components/table-component/table-component.component';


@NgModule({
  declarations: [
    AppComponent,
    TableWrapperComponent,
    TableComponentComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
