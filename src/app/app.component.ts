import { Component, ElementRef, ContentChild, Renderer2, AfterContentInit, ViewChild } from '@angular/core';

@Component({
  selector: 'ngp-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  @ViewChild('alert1') alert1: ElementRef;
  @ViewChild('alert2') alert2: ElementRef;
  @ViewChild('alert3') alert3: ElementRef;

  constructor(private rend2: Renderer2) {}

  showAlert1() {
    // IMPORTANT!: The issue is that you are controlling with different buttons and events the same part of the view
    
    this.rend2.setStyle(this.alert1.nativeElement, 'display', 'block');
  }
  showAlert2() {
    this.rend2.setStyle(this.alert2.nativeElement, 'display', 'block');
  }
  showAlert3() {
    this.rend2.setStyle(this.alert3.nativeElement, 'display', 'block');
  }

}
