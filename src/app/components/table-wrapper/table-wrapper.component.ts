import { Component, OnInit, ElementRef, Output, EventEmitter, Renderer2, ViewChild, AfterViewInit, ContentChild } from '@angular/core';

import { TableComponentComponent } from "../table-component/table-component.component";

@Component({
  selector: 'table-wrapper',
  templateUrl: './table-wrapper.component.html',
  styleUrls: ['./table-wrapper.component.scss']
})
export class TableWrapperComponent {

  // Buttons triggering the actions events
  @ContentChild('primaryButton') primaryButton: ElementRef;
  @ContentChild('secondaryButton') secondaryButton: ElementRef;

  //Get the table reference
  @ViewChild(TableComponentComponent) table: TableComponentComponent;

  // Title for the second demo custom change style event
  @ContentChild('title') title: ElementRef;

  constructor(private rend2: Renderer2) { }

  ngAfterContentInit() {
    // This Lifecycle hook shows all the Content Projected
    // This events ocurrs at same time as the event in the app.component using the direct element event handler
    this.rend2.listen(this.primaryButton.nativeElement, 'click', () => {
      alert('Button 1 pressed')
    });
  }

  ngAfterViewInit() {
    // This Lifecycle hook shows two tables
    console.log(this.table);
  }



}
